<?php

namespace TrueTech\Health\Checks;

use Illuminate\Support\Str;

class Result
{
    /** @var array<string, string|int|bool> */
    public array $meta = [];

    private Check $check;

    public string $status;
    private ?string $shortSummary;
    public ?string $message;

    public static function make(string $message = ''): self
    {
        return new self('ok', $message);
    }

    public function __construct(
        string $status,
        string $notificationMessage = null,
        string $shortSummary = null
    ) {
        $this->message = $notificationMessage;
        $this->shortSummary = $shortSummary;
        $this->status = $status;
    }

    public function shortSummary(string $shortSummary): self
    {
        $this->shortSummary = $shortSummary;

        return $this;
    }

    public function getShortSummary(): string
    {
        if (! empty($this->shortSummary)) {
            return $this->shortSummary;
        }

        return Str::of($this->status)->snake()->replace('_', ' ')->title();
    }

    public function check(Check $check): self
    {
        $this->check = $check;

        return $this;
    }

    public function message(string $notificationMessage): self
    {
        $this->message = $notificationMessage;

        return $this;
    }

    public function ok(string $message = null): self
    {
        $this->message = $message;

        $this->status = 'ok';

        return $this;
    }

    public function warning(string $message = null): self
    {
        $this->message = $message;

        $this->status = 'warning';

        return $this;
    }

    public function failed(string $message = null): self
    {
        $this->message = $message;

        $this->status = 'failed';

        return $this;
    }

    /** @param  array<string, mixed>  $meta */
    public function meta(array $meta): self
    {
        $this->meta = $meta;

        return $this;
    }
}
