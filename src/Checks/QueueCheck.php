<?php

namespace TrueTech\Health\Checks;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Psr\SimpleCache\InvalidArgumentException;
use TrueTech\Health\HeartbeatJob;

class QueueCheck extends Check
{

    protected string $cacheKey = 'health:checks:queue:latestHeartbeatAt';

    protected ?string $cacheStoreName = null;

    protected int $heartbeatMaxAgeInMinutes = 3;

    public function useCacheStore(string $cacheStoreName): self
    {
        $this->cacheStoreName = $cacheStoreName;

        return $this;
    }

    public function getCacheStoreName(): string
    {
        return $this->cacheStoreName ?? 'file';
    }

    public function cacheKey(string $cacheKey): self
    {
        $this->cacheKey = $cacheKey;

        return $this;
    }

    public function heartbeatMaxAgeInMinutes(int $heartbeatMaxAgeInMinutes): self
    {
        $this->heartbeatMaxAgeInMinutes = $heartbeatMaxAgeInMinutes;

        return $this;
    }

    public function getCacheKey(): string
    {
        return $this->cacheKey;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function run(): Result
    {
        $result = Result::make()
            ->ok();

        dispatch(new HeartbeatJob());

        $lastHeartbeatTimestamp = Cache::store($this->getCacheStoreName())->get($this->cacheKey);

        if (! $lastHeartbeatTimestamp) {
            return $result->failed('The queue did not run yet.');
        }

        $latestHeartbeatAt = Carbon::createFromTimestamp($lastHeartbeatTimestamp);

        $minutesAgo = $latestHeartbeatAt->diffInMinutes() + 1;

        if ($minutesAgo > $this->heartbeatMaxAgeInMinutes) {
            return $result->failed("The last run of the job was more than {$minutesAgo} minutes ago.");
        }

        return $result;
    }
}
