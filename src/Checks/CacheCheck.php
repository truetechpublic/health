<?php

namespace TrueTech\Health\Checks;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class CacheCheck extends Check
{
    protected ?string $driver = null;

    public function driver(string $driver): self
    {
        $this->driver = $driver;

        return $this;
    }

    public function run(): Result
    {
        $driver = $this->driver ?? $this->defaultDriver();

        $result = Result::make()->meta([
            'driver' => $driver,
        ]);

        try {
            return $this->canWriteValuesToCache($driver)
                ? $result->ok()
                : $result->failed('Could not set or retrieve an application cache value.');
        } catch (\Throwable $exception) {
            return $result->failed("An exception occurred with the application cache: `{$exception->getMessage()}`");
        }
    }

    protected function defaultDriver(): string
    {
        return config('cache.default', 'file');
    }

    protected function canWriteValuesToCache(string $driver): bool
    {
        $expectedValue = Str::random(5);

        Cache::store($driver)->put('laravel-health:check', $expectedValue, 10);

        $actualValue = Cache::store($driver)->get('laravel-health:check');

        return $actualValue === $expectedValue;
    }
}
