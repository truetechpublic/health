<?php

namespace TrueTech\Health\Facades;

use TrueTech\Health\Check;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Facade;

/**
 * @method static Collection registeredChecks()
 * @method static \TrueTech\Health\Health checks(array $checks)
 * @method static \TrueTech\Health\Health runCheck(Check $check)
 */
class Health extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'health';
    }
}
