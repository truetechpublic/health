<?php

namespace TrueTech\Health;

use TrueTech\Health\Checks\Check;
use TrueTech\Health\Checks\QueueCheck;
use TrueTech\Health\Checks\Result;
use TrueTech\Health\Checks\ScheduleCheck;
use Carbon\Carbon;
use Closure;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Http\JsonResponse;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class HealthServiceProvider extends ServiceProvider
{

    /**
     * Boot the service provider.
     */
    public function boot(): void
    {

        // register scheduler
        $this->callAfterResolving(Schedule::class, function (Schedule $schedule) {
            $schedule->call(function () {
                $this->handleScheduleCheckHeartbeat();
            })->everyMinute();
        });

        // register job event
        Event::listen(JobProcessed::class, function () {
            $this->handleQueueCheckHeartbeat();
        });

        // register health route
        Route::get('/health', $this->registerHealthRoute());
    }

    public function register()
    {
        $this->app->singleton('health', function () {
            return new Health();
        });
    }

    private function registerHealthRoute(): Closure
    {
        return function () {
            $healthResult = app('health')->registeredChecks()
                ->mapWithKeys(function (Check $check): array {
                    $result = $check->shouldRun()
                        ? Health::runCheck($check)
                        : (new Result('skipped'))->check($check);

                    return [$check->getName() => $result];
                });

            return response()->json($healthResult);
        };
    }

    private function handleQueueCheckHeartbeat() : void {

        /** @var QueueCheck|null $queueCheck */
        $queueCheck = app('health')->registeredChecks()->first(
            fn (Check $check) => $check instanceof QueueCheck
        );

        if (! $queueCheck) {
            return;
        }

        $cacheKey = $queueCheck->getCacheKey();

        if (!$cacheKey) {
            return;
        }

        Cache::store($queueCheck->getCacheStoreName())->set($cacheKey, Carbon::now()->timestamp);
    }

    private function handleScheduleCheckHeartbeat() : void {

        /** @var ScheduleCheck|null $scheduleCheck */
        $scheduleCheck = app('health')->registeredChecks()->first(
            fn (Check $check) => $check instanceof ScheduleCheck
        );

        if (! $scheduleCheck) {
            return;
        }

        $cacheKey = $scheduleCheck->getCacheKey();

        if (! $cacheKey) {
            return;
        }

        Cache::store($scheduleCheck->getCacheStoreName())->set($cacheKey, Carbon::now()->timestamp);

    }
}
