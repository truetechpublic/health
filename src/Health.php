<?php

namespace TrueTech\Health;

use TrueTech\Health\Checks\Check;
use TrueTech\Health\Checks\Result;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class Health
{

    /** @var array<int, Check> */
    protected array $checks = [];


    public function checks(array $checks): Health
    {
        $this->checks = array_merge($this->checks, $checks);

        return $this;
    }

    /** @return Collection<int, Check> */
    public function registeredChecks(): Collection
    {
        return collect($this->checks);
    }

    public static function runCheck(Check $check): Result
    {

        try {
            $result = $check->run();
        } catch (\Exception $exception) {
            Log::error('Health check crashed' . $exception->getMessage());
            $result = $check->markAsCrashed();
        }

        $result->check($check);

        return $result;
    }
}
